﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

public enum BannerPos
{
    Bottom,
    Top
}
public enum AdStatus
{
    Showing,
    Finished
}
public class AdManager : MonoBehaviour
{
    public bool IsAdShowed = false;

    private void Awake()
    {
        GameManager.My_AdManager = this;
    }
    private void Start()
    {
        StartAPPLOVIN();

        StartUnityAds();

        StartCoroutine(CheckAdsLoadedStatus());

        ActivateBanner(BannerPos.Bottom);
    }
    private void SetAdShowingStatus(AdStatus adStatus)
    {
        GameManager.My_SoundManager.SetAudioLevel((float)adStatus);
    }

    public void ApplyInterstitial()
    {
        GameManager.My_DeathPanelManager.WatchDone();
    }
    public void ApplyReward()
    {
        GameManager.My_DeathPanelManager.WatchDone();
    }
    public void AdShowFailed()
    {
        GameManager.ActivateStartButton(StartType.Restart);
    }
    public void AdsClosedWithoutReward()
    {
        
    }
    public bool IsInterstitialReady()
    {
        if (AppLovin.HasPreloadedInterstitial())
            return true;

        if (Advertisement.IsReady())
            return true;

        return false;
    }

    public bool IsRewardedReady()
    {
        if (AppLovin.IsIncentInterstitialReady())
            return true;

        if (Advertisement.IsReady("rewardedVideo"))
            return true;

        return false;
    }
    public bool IsAnyVideoAdReady()
    {
        return (IsInterstitialReady() || IsRewardedReady());
    }

    public void Show_INTERSTITIAL()
    {
        if (APPLOVIN_Show_Interstitial())
        {
            GameManager.DeActivateStartButton();
            IsAdShowed = true;
            return;
        }

        if (UNITY_ShowInterstitial())
        {
            GameManager.DeActivateStartButton();
            IsAdShowed = true;
            return;
        }

    }
    public void Show_REWARDED()
    {
        if (APPLOVIN_Show_Rewarded())
        {
            GameManager.DeActivateStartButton();
            IsAdShowed = true;
            return;
        }

        if (UNITY_ShowRewardedAd())
        {
            GameManager.DeActivateStartButton();
            IsAdShowed = true;
            return;
        }
    }

    public void ActivateBanner(BannerPos bannerPos)
    {
        if(bannerPos == BannerPos.Bottom)
            AppLovin.ShowAd(AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_BOTTOM);
        else
            AppLovin.ShowAd(AppLovin.AD_POSITION_CENTER, AppLovin.AD_POSITION_TOP);
    }
    public void DeActivateBanner()
    {
        AppLovin.HideAd();
    }


    private IEnumerator CheckAdsLoadedStatus()
    {
        while(true)
        {
            if (!AppLovin.HasPreloadedInterstitial())
                AppLovin.PreloadInterstitial();

            if (!AppLovin.IsIncentInterstitialReady())
                AppLovin.LoadRewardedInterstitial();

            if (!Advertisement.IsReady() || !Advertisement.IsReady("rewardedVideo"))
                StartUnityAds();

            yield return new WaitForSeconds(30f);
        }
    }



    #region APPLOVIN

    private void StartAPPLOVIN()
    {
        AppLovin.SetSdkKey("secret:))");
        AppLovin.InitializeSdk();

        AppLovin.SetUnityAdListener(this.gameObject.name);

        AppLovin.SetTestAdsEnabled("false");

        //AppLovin.SetRewardedVideoUsername("user");

        AppLovin.PreloadInterstitial();
        AppLovin.LoadRewardedInterstitial();
    }

    private bool APPLOVIN_Show_Interstitial()
    {
        if (AppLovin.HasPreloadedInterstitial())
        {
            AppLovin.ShowInterstitial();
            return true;
        }
        else
        {
            AppLovin.PreloadInterstitial();
            return false;
        }
    }
    private bool APPLOVIN_Show_Rewarded()
    {
        if (AppLovin.IsIncentInterstitialReady())
        {
            AppLovin.ShowRewardedInterstitial();
            return true;
        }
        else
        {
            AppLovin.LoadRewardedInterstitial();

            return false;
        }
    }
    void onAppLovinEventReceived(string ev)
    {
        Log(ev);

        //interstitial
        if (ev.Contains("DISPLAYEDINTER"))
        {
            // An ad was shown.  Pause the game.
        }
        else if (ev.Contains("HIDDENINTER"))
        {
            ApplyInterstitial();

            // If you're using PreloadInterstitial/HasPreloadedInterstitial, make a preload call here.
            AppLovin.PreloadInterstitial();
        }
        else if (ev.Contains("LOADEDINTER"))
        {
            // An interstitial ad was successfully loaded.
        }
        else if (string.Equals(ev, "LOADINTERFAILED"))
        {
            // An interstitial ad failed to load.
        }

        //rewarded
        if (ev.Contains("REWARDAPPROVEDINFO"))
        {
            AppLovin.LoadRewardedInterstitial();
        }
        else if (ev.Contains("LOADEDREWARDED"))
        {
            // A rewarded video was successfully loaded.
        }
        else if (ev.Contains("LOADREWARDEDFAILED"))
        {
            // A rewarded video failed to load.
        }
        else if (ev.Contains("HIDDENREWARDED"))
        {
            // A rewarded video was closed.  Preload the next rewarded video.

            ApplyReward();
            AppLovin.LoadRewardedInterstitial();
        }
        else if (ev.Contains("USERCLOSEDEARLY "))
        {
            // A rewarded video was closed.  Preload the next rewarded video.

            ApplyReward();
            AppLovin.LoadRewardedInterstitial();
        }
    }


    private void Log(string message)
    {
        Debug.Log("APPLOVIN: " + message);
    }

    #endregion

    #region UNITY


    private void StartUnityAds()
    {
        string unityGameId = "";

#if UNITY_ANDROID

        unityGameId = "secret:))";

#elif UNITY_IPHONE

            unityGameId = "secret:))";

#else
        unityGameId = "";

#endif
        Advertisement.Initialize(unityGameId, false);
    }
    private bool UNITY_ShowInterstitial()
    {
        if (Advertisement.IsReady("video"))
        {
            var options = new ShowOptions { resultCallback = INTERSTITIAL_HandleShowResult };
            Advertisement.Show("video", options);
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool UNITY_ShowRewardedAd()
    {
        if (Advertisement.IsReady("rewardedVideo"))
        {
            var options = new ShowOptions { resultCallback = REWARDED_HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            return true;
        }
        else
        {
            return false;
        }
    }
    private void INTERSTITIAL_HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");

                ApplyInterstitial();
                SetAdShowingStatus(AdStatus.Finished);
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                ApplyInterstitial();
                SetAdShowingStatus(AdStatus.Finished);

                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                AdShowFailed();
                break;
        }
    }

    private void REWARDED_HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");

                ApplyReward();
                SetAdShowingStatus(AdStatus.Finished);
                //
                // YOUR CODE TO REWARD THE GAMER
                // Give coins etc.
                break;
            case ShowResult.Skipped:
                ApplyReward();
                SetAdShowingStatus(AdStatus.Finished);

                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }
    }


    #endregion

}
