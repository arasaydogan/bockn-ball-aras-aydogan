﻿using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public enum Position
{
    Upper,
    Lower
}

public class WallManager : MonoBehaviour
{
    #region DEFINITIONS


    public GameObject CubePrefab;
    public Transform LowerWallParent, UpperWallParent;
    public Color32[] CurrentColors { get; private set; }

    [SerializeField]
    private Color32 WallColor00, WallColor01, WallColor10, WallColor11, WallColor20, WallColor21;// WallColor30, WallColor31;
    //private Color32 CameraBackColor0, CameraBackColor1, CameraBackColor2;
    private Color32[][] allWallColors;
    private int currentColorIndex = 0;


    private Transform[] wallParentsArray;
    private List<GameObject>[] cubePoolListArray;
    private float[] cachedYPosArray;

    private Transform cachedTransform;

    private int wallZpos = 0;


    #endregion

    #region AWAKE_START

    private void Awake()
    {
        GameManager.My_WallManager = this;

        wallParentsArray = new Transform[2] { LowerWallParent, UpperWallParent };
        cubePoolListArray = new List<GameObject>[2] { new List<GameObject>(), new List<GameObject>() };
        cachedYPosArray = new float[2] { LowerWallParent.localPosition.y, UpperWallParent.localPosition.y };
        CreateCubePool(Position.Lower, 50);
        CreateCubePool(Position.Upper, 50);

        allWallColors = new Color32[3][]
        {
            new Color32[2] { WallColor00, WallColor01 },
            new Color32[2] { WallColor10, WallColor11 },
            new Color32[2] { WallColor20, WallColor21 },
        };

        CurrentColors = allWallColors[currentColorIndex];
    }

    private void Start()
    {
        GameManager.My_CameraManager.ChangeCameraBackgroundColor(CurrentColors[0], 0);
    }

    #endregion

    public void Restart()
    {
        wallZpos = 0;

        for (int i = 0; i < cubePoolListArray.Length; i++)
        {
            for (int j = 0; j < cubePoolListArray[i].Count; j++)
            {
                cubePoolListArray[i][j].SetActive(false);
            }
        }
    }
    public void CheckExpandWalls(float player_Z_pos)
    {
        if (player_Z_pos + 15 <= wallZpos)
            return;

        wallZpos++;
        ExpandWallOps(Position.Lower);
        ExpandWallOps(Position.Upper);

        if (wallZpos % 40 == 39)
        {
            currentColorIndex++;

            if (currentColorIndex == allWallColors.Length)
                currentColorIndex = 0;

            CurrentColors = allWallColors[currentColorIndex];

            GameManager.My_CameraManager.ChangeCameraBackgroundColor(CurrentColors[0], wallZpos);
        }            
    }

    // avoid code duplication for every Position
    private void ExpandWallOps(Position position)
    {
        cachedTransform = GetCubeFromPool(position);
        cachedTransform.localPosition = new Vector3(0, cachedYPosArray[(int)position] + Random.Range(-1f, 1f), wallZpos);

        cachedTransform.GetComponent<MeshRenderer>().material.color = CurrentColors[wallZpos % 2];
        cachedTransform.gameObject.SetActive(true);
    }

    #region POOLING

    // creating a generic method easy to impelement Upper and Lower options
    private void CreateCubePool(Position position, int count = 20)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject obj = Instantiate(CubePrefab);
            obj.transform.SetParent(wallParentsArray[(int)position]);
            cubePoolListArray[(int)position].Add(obj);
            obj.gameObject.SetActive(false);
        }
    }

    private Transform GetCubeFromPool(Position position)
    {
        // get cube from the pool
        for (int i = 0; i < cubePoolListArray[(int)position].Count; i++)
        {
            if (!cubePoolListArray[(int)position][i].activeInHierarchy)
            {
                return cubePoolListArray[(int)position][i].transform;
            }
        }

        // couldn't get, expand the pool
        CreateCubePool(position);

        // re-try to get
        for (int i = 0; i < cubePoolListArray[(int)position].Count; i++)
        {
            if (!cubePoolListArray[(int)position][i].activeInHierarchy)
            {
                return cubePoolListArray[(int)position][i].transform;
            }
        }

        // weird error arised, return null, check this
        Debug.LogError("Cannot get or expand the pool");
        return null;
    }

    #endregion
}
