﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SoundManager : MonoBehaviour
{
    public AudioSource MainThemeSource { get; private set; }
    public AudioSource BoosterSource { get; private set; }
    public AudioSource ReactionSource { get; private set; }

    [SerializeField]
    private float MainThemeSoundLevel;
    [SerializeField]
    private AudioClip MainTheme, BoosterSound;
    [SerializeField]
    private AudioClip[] ReactionSounds;

    private void Awake()
    {
        GameManager.My_SoundManager = this;

        MainThemeSoundLevel = 0.4f;

        MainThemeSource = gameObject.AddComponent<AudioSource>();
        MainThemeSource.volume = MainThemeSoundLevel;
        MainThemeSource.loop = true;
        MainThemeSource.playOnAwake = false;

        BoosterSource = gameObject.AddComponent<AudioSource>();
        BoosterSource.volume = 1f;
        BoosterSource.loop = false;
        BoosterSource.playOnAwake = false;

        ReactionSource = gameObject.AddComponent<AudioSource>();
        ReactionSource.volume = 1f;
        ReactionSource.loop = false;
        ReactionSource.playOnAwake = false;
    }
    private void Start()
    {
        MainThemeSource.clip = MainTheme;
        MainThemeSource.Play();
    }
    public void SetAudioLevel(float count)
    {
        count = Mathf.Clamp(count, 0f, MainThemeSoundLevel);
        MainThemeSource.volume = count;
    }

    public void PlayBoosterSound()
    {
        if (BoosterSource.isPlaying)
            BoosterSource.Stop();

        BoosterSource.clip = BoosterSound;
        BoosterSource.Play();
    }
    public void PlayReactionSound()
    {
        if (ReactionSource.isPlaying)
            ReactionSource.Stop();

        ReactionSource.clip = ReactionSounds[Random.Range(0, ReactionSounds.Length)];
        ReactionSource.Play();

    }
}
