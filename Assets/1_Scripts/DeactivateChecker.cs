﻿using UnityEngine;

public class DeactivateChecker : MonoBehaviour
{
    private float cubeZPos;

    private void OnEnable()
    {
        cubeZPos = transform.localPosition.z;

        Invoke("CheckDeactivate", 1);
    }

    private void CheckDeactivate()
    {
        if(GameManager.PlayerZPos > cubeZPos + 10)
        {
            gameObject.SetActive(false);
            return;
        }

        Invoke("CheckDeactivate", 1);
    }
}
