﻿using UnityEngine;

public enum SceneIndex
{
    Intro,
    Game
}
public enum StartType
{
    Restart,
    Revive
}
public static class GameManager
{
    public static int BoosterPoint { get { return 10; } }
    public static float Score { get; set; }
    public static float Timer { get; set; }
    public static float DeathTimer { get; set; }

    public static float PlayerZPos { get; set; }
    public static bool IsGameStarted { get; private set; }

    public static WallManager My_WallManager { get; set; }
    public static BoosterSpawner My_BoosterSpawner { get; set; }    
    public static PlayerManager My_PlayerManager { get; set; }
    public static CameraManager My_CameraManager { get; set; }
    public static UIManager My_UIManager { get; set; }
    public static DeathPanelManager My_DeathPanelManager { get; set; }
    public static SoundManager My_SoundManager { get; set; }
    public static ParticleManager My_ParticleManager { get; set; }
    public static AdManager My_AdManager { get; set; }

    public static float Gravity { get { return -0.2f; } private set { } }

    public static void Death()
    {
        IsGameStarted = false;
        
        My_PlayerManager.DeActivatePlayer();
        My_DeathPanelManager.ActivateDeathPanel();
        My_ParticleManager.StartParticle.SetActive(true);
    }
    public static void ReStart()
    {
        My_AdManager.IsAdShowed = false;

        Score = Timer = DeathTimer = 0f;

        My_WallManager.Restart();
        My_BoosterSpawner.Restart();
        My_PlayerManager.ActivatePlayer();
        My_UIManager.FirstStartPanel.SetActive(false);
        DeActivateStartButton();
        My_DeathPanelManager.DeActivateDeathPanel();
        My_ParticleManager.StartParticle.SetActive(false);
        My_ParticleManager.DeActivateBlast();

        IsGameStarted = true;
    }
    public static void Revive()
    {
        My_AdManager.IsAdShowed = false;

        Timer = DeathTimer = 0f;

        My_PlayerManager.ActivatePlayer(true);
        My_UIManager.FirstStartPanel.SetActive(false);
        DeActivateStartButton();
        My_DeathPanelManager.DeActivateDeathPanel();
        My_ParticleManager.StartParticle.SetActive(false);
        My_ParticleManager.DeActivateBlast();

        IsGameStarted = true;
    }
    public static void DeActivateStartButton()
    {
        My_UIManager.StartButton.gameObject.SetActive(false);
    }
    public static void ActivateStartButton(StartType startType)
    {
        My_UIManager.StartButton.onClick.RemoveAllListeners();

        if (startType == StartType.Restart)
            My_UIManager.StartButton.onClick.AddListener(ReStart);
        else
            My_UIManager.StartButton.onClick.AddListener(Revive);

        My_UIManager.StartButton.gameObject.SetActive(true);
    }
    public static void SetTotalScore()
    {
        My_UIManager.SetTextScore(Score);
    }

    public static Color32 GetRandomCurrentColor()
    {
        Color32 newColor = My_WallManager.CurrentColors[Random.Range(0, My_WallManager.CurrentColors.Length)];
        newColor.a = 255;
        return newColor;
    }
    public static int GetHighScore()
    {
        return PlayerPrefs.GetInt("highscore", 0);
    }
    public static bool CheckHighScore()
    {
        if((int)Score > PlayerPrefs.GetInt("highscore", 0))
        {
            PlayerPrefs.SetInt("highscore", (int)Score);
            return true;
        }
        else
        {
            return false;
        }
    }

}
