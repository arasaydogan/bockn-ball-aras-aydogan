﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoosterSpawner : MonoBehaviour
{
    public GameObject[] BoosterPrefabs;
    public GameObject BoosterSparkPrefab;

    private List<List<GameObject>> boostersPool;
    private List<GameObject> sparksPool;

    private Transform managerT;

    private float lastBoosterZpos, boosterRandomSize = 0;
    private Vector3 posVector = Vector3.zero;

    private ParticleSystem.MainModule particleSystemMain_Edge;// particleSystemMain_Middle


    private void Awake()
    {
        GameManager.My_BoosterSpawner = this;

        managerT = transform;

        boostersPool = new List<List<GameObject>>();
        sparksPool = new List<GameObject>();
        CreateBoosterPool();
        CreateBoosterSparkPool();
    }

    public void Restart()
    {
        lastBoosterZpos = 4;

        for (int i = 0; i < boostersPool.Count; i++)
        {
            for (int k = 0; k < boostersPool[i].Count; k++)
            {
                boostersPool[i][k].SetActive(false);
            }
        }
    }
    public void CheckBoosterSpawn(float player_Z_pos)
    {
        if (player_Z_pos + 8 < lastBoosterZpos)
            return;

        lastBoosterZpos += Random.Range(2f, 8f);
        ActivateBooster(lastBoosterZpos);
    }
    public async void ActivateBoosterSpark(Vector3 pos)
    {
        Transform sparkT = GetSparkFromPool();
        sparkT.localPosition = pos;
        sparkT.gameObject.SetActive(true);

        await Task.Delay(TimeSpan.FromSeconds(2f));

        if(sparkT != null)
            sparkT.gameObject.SetActive(false);
    }
    private void ActivateBooster(float zPos)
    {
        Transform boosterT = GetBoosterFromPool(Random.Range(0, BoosterPrefabs.Length));
        posVector.y = Random.Range(-2f, 2f);
        posVector.z = zPos;
        boosterT.localPosition = posVector;
        boosterRandomSize = Random.Range(0.5f, 1f);
        boosterT.GetChild(0).localScale = new Vector3(boosterRandomSize, boosterRandomSize, boosterRandomSize);

        Color32 newColor = GameManager.GetRandomCurrentColor();

        particleSystemMain_Edge = boosterT.GetChild(0).GetComponent<ParticleSystem>().main;
        particleSystemMain_Edge.startColor = (Color)GameManager.GetRandomCurrentColor();

        //particleSystemMain_Middle = boosterT.GetChild(1).GetComponent<ParticleSystem>().main;
        //particleSystemMain_Middle.startColor = (Color)GameManager.GetRandomCurrentColor();

        boosterT.gameObject.SetActive(true);
    }
    private void CreateBoosterPool(int count = 10)
    {
        for (int i = 0; i < BoosterPrefabs.Length; i++)
        {
            boostersPool.Add(new List<GameObject>());

            for (int k = 0; k < count; k++)
            {
                GameObject obj = Instantiate(BoosterPrefabs[i]);
                obj.transform.SetParent(managerT);
                boostersPool[i].Add(obj);
                obj.SetActive(false);
            }
        }
    }
    private void CreateBoosterSparkPool(int count = 20)
    {
        for (int i = 0; i < BoosterPrefabs.Length; i++)
        {
            GameObject obj = Instantiate(BoosterSparkPrefab);
            obj.transform.SetParent(managerT);
            sparksPool.Add(obj);
            obj.SetActive(false);
        }
    }

    private Transform GetBoosterFromPool(int type)
    {        
        for (int i = 0; i < boostersPool[type].Count; i++)
        {
            if (!boostersPool[type][i].activeInHierarchy)
            {
                return boostersPool[type][i].transform;
            }
        }

        // couldn't get, expand the pool
        CreateBoosterPool();

        // re-try to get
        for (int i = 0; i < boostersPool[type].Count; i++)
        {
            if (!boostersPool[type][i].activeInHierarchy)
            {
                return boostersPool[type][i].transform;
            }
        }

        // weird error arised, return null, check this
        Debug.LogError("Cannot get or expand the pool");
        return null;
    }
    private Transform GetSparkFromPool()
    {
        for (int i = 0; i < sparksPool.Count; i++)
        {
            if (!sparksPool[i].activeInHierarchy)
            {
                return sparksPool[i].transform;
            }
        }

        // couldn't get, expand the pool
        CreateBoosterSparkPool();

        // re-try to get
        for (int i = 0; i < sparksPool.Count; i++)
        {
            if (!sparksPool[i].activeInHierarchy)
            {
                return sparksPool[i].transform;
            }
        }

        // weird error arised, return null, check this
        Debug.LogError("Cannot get or expand the pool");
        return null;
    }
}
