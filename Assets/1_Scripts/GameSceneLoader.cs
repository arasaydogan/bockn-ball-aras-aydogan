﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneLoader : MonoBehaviour
{
    private void Start()
    {
        SceneManager.LoadScene((int)SceneIndex.Game);
    }
}
