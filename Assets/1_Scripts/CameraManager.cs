﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    [SerializeField]
    private float lerpSpeed0, lerpSpeed1, moveTowSpeed0;

    private Transform camT;
    private Camera cam;
    private Color32 currentColor, newColor;

    private Vector3 firstPos, cameraMovePos, gap, playerUIPosCurrent, playerUIPosNew, aimPos = Vector3.zero;

    private void Awake()
    {
        GameManager.My_CameraManager = this;

        camT = transform;
        cam = GetComponent<Camera>();

        firstPos = camT.localPosition;

    }
    private void Start()
    {
        gap = firstPos - GameManager.My_PlayerManager.transform.localPosition;
    }

    private void LateUpdate()
    {
        if (!GameManager.IsGameStarted)
            return;

        aimPos = GameManager.My_PlayerManager.PlayerT.localPosition;

        //cameraMovePosLayer0.y = Mathf.LerpUnclamped(cameraMovePosLayer0.y, My_PlayerManager.Pos.y, Time.deltaTime * lerpSpeed0);

        //cameraMovePos.y = aimPos.y;

        cameraMovePos.z = Mathf.LerpUnclamped(cameraMovePos.z, aimPos.z, Time.deltaTime * lerpSpeed0);
        cameraMovePos.z = Mathf.MoveTowards(cameraMovePos.z, aimPos.z, Time.deltaTime * moveTowSpeed0);

        camT.localPosition = cameraMovePos + gap;
        camT.LookAt(aimPos);
    }

    public void ResetCameraPos()
    {
        aimPos.y = 0;
        camT.LookAt(aimPos);
    }

    public void ChangeCameraBackgroundColor(Color32 new_color, float effect_pos)
    {
        newColor = new_color;

        StartCoroutine(ChangeColor_CR(effect_pos));
    }

    private IEnumerator ChangeColor_CR(float effect_pos)
    {
        while(GameManager.PlayerZPos + 2 < effect_pos)
        {
            yield return new WaitForSeconds(0.3f);
        }

        float timer = 3f;

        while(timer > 0)
        {
            currentColor = Color32.LerpUnclamped(currentColor, newColor, Time.deltaTime * 0.6f);
            cam.backgroundColor = currentColor;
            timer -= Time.deltaTime;

            yield return null;
        }
    }
}
