﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviour
{
    public GameObject StartParticle, BlastParticle;

    private Transform blastT;

    private void Awake()
    {
        GameManager.My_ParticleManager = this;

        blastT = BlastParticle.transform;
        BlastParticle.SetActive(false);
    }

    private void Start()
    {
        StartParticle.SetActive(true);
    }

    public void ActivateBlast(Vector3 pos)
    {
        blastT.localPosition = pos;
        blastT.gameObject.SetActive(true);
    }
    public void DeActivateBlast()
    {
        blastT.gameObject.SetActive(false);
    }
}
