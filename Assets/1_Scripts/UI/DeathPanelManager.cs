﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DeathPanelManager : MonoBehaviour
{
    public GameObject MainParent, WatchPanel;
    public Text HighScoreText, NewScoreText, RestartText;
    public Image WatchFillTimerImage;
    public Button WatchButton;
    public GameObject WatchImage;

    private float watch_timer, watch_timer_base;

    private void Awake()
    {
        GameManager.My_DeathPanelManager = this;


        watch_timer_base = 4.5f;

    }
    private void Start()
    {

        DeActivateDeathPanel();
    }
    public void DeActivateDeathPanel()
    {
        MainParent.SetActive(false);
        GameManager.My_UIManager.UIScoreText.gameObject.SetActive(true);
    }

    public void ActivateDeathPanel()
    {
        MainParent.SetActive(true);

        GameManager.My_UIManager.UIScoreText.gameObject.SetActive(false);

        HighScoreText.text = "PREVIOUS HIGH SCORE" + "\n" + GameManager.GetHighScore();

        if (GameManager.CheckHighScore())
        {
            NewScoreText.text = "<size=300>" + "NEW HIGH SCORE!" + "</size>" + "\n" + (int)GameManager.Score;
        }
        else
        {
            NewScoreText.text = "SCORE" + "\n" + (int)GameManager.Score;
        }

        if (GameManager.DeathTimer < 5f || GameManager.Score < GameManager.BoosterPoint * 5)
        {
            RestartText.text = "TAP TO RESTART";
            SetOpenWatchPanel(false);
            return;
        }

        WatchImage.SetActive(GameManager.My_AdManager.IsAnyVideoAdReady());
        WatchButton.onClick.RemoveAllListeners();

        if (GameManager.CheckHighScore())
        {
            if (GameManager.Score > GameManager.BoosterPoint * 200 && GameManager.My_AdManager.IsRewardedReady())
                WatchButton.onClick.AddListener(GameManager.My_AdManager.Show_REWARDED);
            else
                WatchButton.onClick.AddListener(GameManager.My_AdManager.Show_INTERSTITIAL);
        }
        else
        {
            if (GameManager.My_AdManager.IsInterstitialReady())
                WatchButton.onClick.AddListener(GameManager.My_AdManager.Show_INTERSTITIAL);
            else
                WatchButton.onClick.AddListener(GameManager.My_AdManager.Show_REWARDED);
        }

        StartCoroutine(WatchTimer_CR());
    }

    private IEnumerator WatchTimer_CR()
    {
        watch_timer = watch_timer_base;

        SetOpenWatchPanel(true);

        while (watch_timer > 0 && !GameManager.My_AdManager.IsAdShowed)
        {
            if (GameManager.My_AdManager.IsAdShowed)
                yield break;

            watch_timer -= Time.deltaTime;

            WatchFillTimerImage.fillAmount = watch_timer / watch_timer_base;

            yield return null;
        }
        if (!GameManager.My_AdManager.IsAdShowed)
        {
            SetOpenWatchPanel(false);
        }
    }

    private void SetOpenWatchPanel(bool willActivate)
    {
        WatchPanel.SetActive(willActivate);

        RestartText.gameObject.SetActive(!willActivate);

        if (willActivate)
        {
            GameManager.DeActivateStartButton();
        }
        else
        {
            GameManager.ActivateStartButton(StartType.Restart);
        }
    }

    public void WatchDone()
    {
        GameManager.My_CameraManager.ResetCameraPos();

        RestartText.text = "TAP TO CONTINUE";
        SetOpenWatchPanel(false);

        GameManager.ActivateStartButton(StartType.Revive);
    }
}
