﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class UIManager : MonoBehaviour
{
    public Transform AnimateTextParent;
    public Text UIScoreText;
    public GameObject CriticalScorePrefab;

    public GameObject FirstStartPanel;
    public Button StartButton;

    public Transform SpeedUpTextParent;
    private Text speedUpText;
    private Color32 textWhiteColor;

    private List<GameObject> critTextPool;
    private string[] reactions;
    private float reactionAnimTimer;
    private Vector2 reactionScale;
    private bool isReacting = false;

    private void Awake()
    {
        GameManager.My_UIManager = this;

        critTextPool = new List<GameObject>();

        GameManager.ActivateStartButton(StartType.Restart);

        SpeedUpTextParent.gameObject.SetActive(false);
        speedUpText = SpeedUpTextParent.GetChild(0).GetComponent<Text>();
        textWhiteColor = Color.white;

        reactionAnimTimer = -1f;
        reactionScale = new Vector2(2f, 2f);
        reactions = new string[3] { "PERFECT!", "NICE JOB!", "AWESOME!" };
    }
    private void Start()
    {
        CreateTextPool();

        FirstStartPanel.SetActive(true);
    }
    public void SetTextScore(float total_count)
    {
        UIScoreText.text = "" + (int)total_count;
    }

    public void AnimateScore(int add_count, Vector3 pos, bool is_bonus)
    {        
        Transform textT = GetTextFromPool();
        Text child_text = textT.GetChild(0).GetComponent<Text>();
        textT.localPosition = pos;
        if (is_bonus)
        {
            child_text.text = "+" + add_count;
            child_text.color = GameManager.GetRandomCurrentColor();
            StartCoroutine(TextAnim_CR(textT, pos, 1f, Random.Range(-100f, 200f), -400f));
        }
        else
        {
            child_text.text = "" + add_count;
            child_text.color = Color.white;
            StartCoroutine(TextAnim_CR(textT, pos, 1f, Random.Range(-100f, -50f), 400f));
        }
    }

    private IEnumerator TextAnim_CR(Transform objT, Vector3 start_pos, float aim_scale, float xBias, float ySpeed)
    {
        objT.gameObject.SetActive(true);

        float anim_timer = 2f;

        Vector2 scale = Vector2.zero;

        //float yRotate = Random.Range(-15f, 15f);
        //float zRotate = Random.Range(-15f, 15f);

        Vector3 moveVector = Vector3.zero;

        while (anim_timer > 0)
        {
            scale.x = Mathf.LerpUnclamped(scale.x, aim_scale, Time.deltaTime * 3);
            scale.y = scale.x;

            moveVector.x += xBias * Time.deltaTime;
            moveVector.y += ySpeed * Time.deltaTime;

            objT.localScale = scale;
            objT.localPosition = moveVector;
            //objT.Rotate(0f, yRotate * Time.deltaTime, zRotate * Time.deltaTime);

            anim_timer -= Time.deltaTime;

            yield return null;
        }

        objT.gameObject.SetActive(false);
    }
    public void ReactionAnim()
    {
        reactionAnimTimer = 2f;
        reactionScale.x = reactionScale.y = 2f;
        speedUpText.color = textWhiteColor;

        if (!isReacting)
        {
            StartCoroutine(ReactionAnim_CR());
        }
    }
    private IEnumerator ReactionAnim_CR()
    {
        GameManager.My_SoundManager.PlayReactionSound();
        speedUpText.text = reactions[Random.Range(0, reactions.Length)];

        SpeedUpTextParent.gameObject.SetActive(true);

        textWhiteColor.a = 255;
        speedUpText.color = textWhiteColor;

        reactionAnimTimer = 2f;
        reactionScale = new Vector2(2f, 2f);

        isReacting = true;

        while (reactionAnimTimer > 0)
        {
            reactionScale.x = Mathf.LerpUnclamped(reactionScale.x, 1f, Time.deltaTime * 2);
            reactionScale.y = reactionScale.x;

            SpeedUpTextParent.localScale = reactionScale;

            reactionAnimTimer -= Time.deltaTime;

            if(reactionAnimTimer < 1f)
            {
                textWhiteColor.a= (byte)Mathf.LerpUnclamped(textWhiteColor.a, 0f, Time.deltaTime * 2);

                speedUpText.color = textWhiteColor;
            }

            yield return null;
        }
        isReacting = false;
        SpeedUpTextParent.gameObject.SetActive(false);
    }
    private void CreateTextPool(int count = 20)
    {
        for (int i = 0; i < 20; i++)
        {
            GameObject obj = Instantiate(CriticalScorePrefab);
            obj.transform.SetParent(AnimateTextParent);
            obj.transform.localScale = Vector3.one;
            critTextPool.Add(obj);
            obj.SetActive(false);
        }
    }
    private Transform GetTextFromPool()
    {
        for (int i = 0; i < critTextPool.Count; i++)
        {
            if (!critTextPool[i].activeInHierarchy)
            {
                return critTextPool[i].transform;
            }
        }

        // couldn't get, expand the pool
        CreateTextPool();

        // re-try to get
        for (int i = 0; i < critTextPool.Count; i++)
        {
            if (!critTextPool[i].activeInHierarchy)
            {
                return critTextPool[i].transform;
            }
        }

        // weird error arised, return null, check this
        Debug.LogError("Cannot get or expand the pool");
        return null;
    }
}
