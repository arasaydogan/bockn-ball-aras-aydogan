﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerManager : MonoBehaviour
{
    public ParticleSystem ShapeParticle, MainTrail, JumpTrail;

    public Transform MainPlayerParent;

    public Vector3 Pos { get; private set; }

    public Transform PlayerT { get; private set; }
    public Rigidbody PlayerRB { get; private set; }

    private ParticleSystem.EmissionModule mainTrailEmission, jumpTrailEmission;
    private Collider collider;

    private float jumpModifier, gravityModifier;
    private int mainTrailEmissionRate, jumpTrailEmissionRate;

    private Vector3 moveVector = Vector3.forward;
    private float startSpeed, moveSpeed, aimSpeed = 300f;
    private float speedUpRatio;
    private bool isSpeeding = false;

    private float bonusModifier;
    private int boosterCounter;

    private void Awake()
    {
        GameManager.My_PlayerManager = this;

        PlayerT = transform;
        PlayerRB = PlayerT.GetComponent<Rigidbody>();
        collider = GetComponent<Collider>();

        mainTrailEmission = MainTrail.emission;
        jumpTrailEmission = JumpTrail.emission;

        mainTrailEmissionRate = 10;
        jumpTrailEmissionRate = 0;
        jumpModifier = 6.5f;

        startSpeed = moveSpeed = aimSpeed = 300f;
        speedUpRatio = 1.01f;
    }


    private void Start()
    {
        SetActivatePlayerShape(false);
    }
    private void ResetPos(Vector3 pos)
    {
        PlayerRB.position = PlayerT.position = pos;
        Pos = pos;
        GameManager.PlayerZPos = pos.z;
    }
    private void FixedUpdate()
    {
        if (!GameManager.IsGameStarted)
            return;

        GameManager.Score += Time.deltaTime;
        GameManager.Timer += Time.deltaTime;
        GameManager.DeathTimer += Time.deltaTime;

        bonusModifier -= Time.deltaTime;
        if (GameManager.Timer > 0.6f)
            gravityModifier -= Time.deltaTime * 1.5f;

        moveVector.y = gravityModifier;

        PlayerT.localRotation = Quaternion.Euler(-gravityModifier * 20, 0f, 0f);

        PlayerRB.velocity = moveVector * (Time.deltaTime * moveSpeed);

        Pos = PlayerT.position;

        GameManager.PlayerZPos = Pos.z;

        GameManager.My_WallManager.CheckExpandWalls(Pos.z);
        GameManager.My_BoosterSpawner.CheckBoosterSpawn(Pos.z);
        GameManager.SetTotalScore();
    }

    private void Update()
    {
        if (!GameManager.IsGameStarted)
            return;

        if (Input.GetMouseButton(0))
        {
            gravityModifier += Time.deltaTime * jumpModifier;

            // instead of changing emission values every frame, just check an integer equality
            if(jumpTrailEmissionRate != 5)
            {
                jumpTrailEmissionRate = 5;
                jumpTrailEmission.rateOverDistance = jumpTrailEmissionRate;
            }

            if (mainTrailEmissionRate != 20)
            {
                mainTrailEmissionRate = 20;

                mainTrailEmission.rateOverDistance = mainTrailEmissionRate;
            }
        }
        else
        {
            if (jumpTrailEmissionRate != 0)
            {
                jumpTrailEmissionRate = 0;

                jumpTrailEmission.rateOverDistance = jumpTrailEmissionRate;
            }

            if (mainTrailEmissionRate != 10)
            {
                mainTrailEmissionRate = 10;

                mainTrailEmission.rateOverDistance = mainTrailEmissionRate;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!GameManager.IsGameStarted)
            return;

        if(other.tag == "Booster")
        {
            GameManager.Score += GameManager.BoosterPoint;

            GameManager.SetTotalScore();
            GameManager.My_UIManager.AnimateScore(GameManager.BoosterPoint, Pos, false);
            GameManager.My_SoundManager.PlayBoosterSound();
            GameManager.My_BoosterSpawner.ActivateBoosterSpark(Pos);

            aimSpeed = moveSpeed * speedUpRatio;
            StartCoroutine(SpeedUp_CR());

            if (bonusModifier > 0f && boosterCounter > 3)
            {
                BonusPoint();
                boosterCounter = 0;
            }
            boosterCounter += 1;
            bonusModifier = 0.6f;
        }
        if (other.tag == "Wall")
        {
            GameManager.Death();

            GameManager.My_ParticleManager.ActivateBlast(Pos);
        }
    }
    private void BonusPoint()
    {
        GameManager.Score += GameManager.BoosterPoint * 2;
        GameManager.SetTotalScore();
        GameManager.My_SoundManager.PlayReactionSound();
        GameManager.My_UIManager.AnimateScore(GameManager.BoosterPoint * 2, Pos, true);

        GameManager.My_UIManager.ReactionAnim();
    }

    private IEnumerator SpeedUp_CR()
    {
        isSpeeding = true;

        while (moveSpeed < aimSpeed)
        {
            moveSpeed = Mathf.LerpUnclamped(moveSpeed, aimSpeed, Time.deltaTime);

            yield return null;
        }
        isSpeeding = false;
    }
    public void ActivatePlayer(bool isRevive = false)
    {
        gravityModifier = 0f;

        if (isRevive)
        {
            ResetPos(new Vector3(Pos.x, 0, Pos.z));

            moveSpeed = aimSpeed = startSpeed + ((moveSpeed - startSpeed) * 0.4f);
        }
        else
        {
            moveSpeed = aimSpeed = startSpeed;
            ResetPos(Vector3.zero);
        }

        collider.enabled = true;
        SetActivatePlayerShape(true);
    }
    public void DeActivatePlayer()
    {
        collider.enabled = false;
        SetActivatePlayerShape(false);
    }
    public void SetActivatePlayerShape(bool is_active)
    {
        ShapeParticle.gameObject.SetActive(is_active);
        MainTrail.gameObject.SetActive(is_active);
        JumpTrail.gameObject.SetActive(is_active);
    }

}
